package lucasextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c LucasConfiguration) (pbv1lucas.LucasServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to lucas: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, nil, err
	}

	return pbv1lucas.NewLucasServiceClient(conn), conn, nil
}
