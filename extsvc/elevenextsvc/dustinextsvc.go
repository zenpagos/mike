package elevenextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pb1eleven "gitlab.com/zenpagos/eleven/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c ElevenConfiguration) (pb1eleven.ElevenServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to eleven: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Errorf("did not connect: %v", err)
		return nil, nil, err
	}

	return pb1eleven.NewElevenServiceClient(conn), conn, nil
}
