package hopperextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pbv1hopper "gitlab.com/zenpagos/hopper/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c HopperConfiguration) (pbv1hopper.HopperServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to hopper: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
		return nil, nil, err
	}

	return pbv1hopper.NewHopperServiceClient(conn), conn, nil
}
