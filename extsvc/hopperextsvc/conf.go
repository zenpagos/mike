package hopperextsvc

type HopperConfiguration struct {
	Host string
	Port int
}
