package dustinextsvc

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	pb1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	"google.golang.org/grpc"
)

func NewGRPCClient(c DustinConfiguration) (pb1dustin.DustinServiceClient, *grpc.ClientConn, error) {
	connectionUri := fmt.Sprintf("%s:%d", c.Host, c.Port)

	log.Tracef("connection to dustin: %s", connectionUri)

	conn, err := grpc.Dial(connectionUri, grpc.WithInsecure())
	if err != nil {
		log.Errorf("did not connect: %v", err)
		return nil, nil, err
	}

	return pb1dustin.NewDustinServiceClient(conn), conn, nil
}
