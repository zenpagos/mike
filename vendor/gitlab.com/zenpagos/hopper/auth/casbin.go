package auth

import (
	"fmt"
	"github.com/casbin/casbin/v2"
	"github.com/casbin/gorm-adapter/v2"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

type AuthorizationManager struct {
	Enforcer *casbin.Enforcer
}

func NewAuthorizationManager(db *gorm.DB) (AuthorizationManagerInterface, error) {
	adapter, err := gormadapter.NewAdapterByDB(db)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", adapter)
	if err != nil {
		log.WithError(err).Error()
		return nil, err
	}

	am := AuthorizationManager{
		Enforcer: enforcer,
	}

	return am, nil
}

func (am AuthorizationManager) formatKeyValue(format string, value string) string {
	return fmt.Sprintf(format, value)
}

func (am AuthorizationManager) generatePolicies(accountKey string, rp RolePermission) [][]string {
	var ps [][]string

	roleKey := am.formatKeyValue(RoleKeyFormatter, rp.Role)
	for _, resourcePermission := range rp.ResourcePermissions {
		for _, permission := range resourcePermission.Permissions {
			ps = append(ps, []string{roleKey, accountKey, resourcePermission.Resource, permission})
		}
	}

	return ps
}

func (am AuthorizationManager) AttachRole(actorID uuid.UUID, accountID uuid.UUID, role string) error {
	actorKey := am.formatKeyValue(ActorKeyFormatter, actorID.String())
	accountKey := am.formatKeyValue(AccountKeyFormatter, accountID.String())
	roleKey := am.formatKeyValue(RoleKeyFormatter, role)

	if _, err := am.Enforcer.AddGroupingPolicy(actorKey, roleKey, accountKey); err != nil {
		log.WithError(err).Error()
		return err
	}

	return nil
}

func (am AuthorizationManager) CreateRolesAndPermissions(accountID uuid.UUID) error {
	accountKey := am.formatKeyValue(AccountKeyFormatter, accountID.String())

	// load the policy from DB.
	if err := am.Enforcer.LoadPolicy(); err != nil {
		log.WithError(err).Error()
		return err
	}

	ownerPolicies := am.generatePolicies(accountKey, OwnerRolePermissions)
	adminPolicies := am.generatePolicies(accountKey, AdminRolePermissions)

	rules := append(ownerPolicies, adminPolicies...)
	if _, err := am.Enforcer.AddPolicies(rules); err != nil {
		log.WithError(err).Error()
		return err
	}

	return nil
}

func (am AuthorizationManager) Authorize(actorID uuid.UUID, accountID uuid.UUID, resource string, action string) (bool, error) {
	// load the policy from DB.
	if err := am.Enforcer.LoadPolicy(); err != nil {
		log.WithError(err).Error()
		return false, err
	}

	actorKey := am.formatKeyValue(ActorKeyFormatter, actorID.String())
	accountKey := am.formatKeyValue(AccountKeyFormatter, accountID.String())

	log.WithFields(log.Fields{
		"actor_key":   actorKey,
		"account_key": accountKey,
		"resource":    resource,
		"action":      action,
	}).Info()

	return am.Enforcer.Enforce(actorKey, accountKey, resource, action)
}
