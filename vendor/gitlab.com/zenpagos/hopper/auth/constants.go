package auth

const (
	ActorKeyFormatter   = "actor:%s"
	RoleKeyFormatter    = "role:%s"
	AccountKeyFormatter = "account:%s"

	OwnerRole = "owner"
	AdminRole = "admin"

	// Resources
	AccountResource         = "account"
	ApiKeyResource          = "api_key"
	BankAccountResource     = "bank_account"
	BalanceResource         = "balance"
	CashOutResource         = "cash_out"
	CheckoutSessionResource = "checkout_session"
	PaymentResource         = "payment"

	// Actions
	CreateAction = "create"
	EditAction   = "edit"
	ListAction   = "list"
	DetailAction = "detail"
	DeleteAction = "delete"
)

var (
	AccountFullAccessRuleGroup = ResourcePermission{
		Resource: AccountResource,
		Permissions: []string{
			DetailAction,
			EditAction,
		},
	}

	ApiKeyFullAccessRuleGroup = ResourcePermission{
		Resource: ApiKeyResource,
		Permissions: []string{
			CreateAction,
			EditAction,
			ListAction,
			DeleteAction,
		},
	}

	BankAccountFullAccessRuleGroup = ResourcePermission{
		Resource: BankAccountResource,
		Permissions: []string{
			CreateAction,
			EditAction,
			ListAction,
			DeleteAction,
		},
	}

	BalanceFullAccessRuleGroup = ResourcePermission{
		Resource: BalanceResource,
		Permissions: []string{
			ListAction,
			DetailAction,
		},
	}

	CashOutFullAccessRuleGroup = ResourcePermission{
		Resource: CashOutResource,
		Permissions: []string{
			CreateAction,
			ListAction,
			DetailAction,
		},
	}

	CheckoutSessionFullAccessRuleGroup = ResourcePermission{
		Resource: CheckoutSessionResource,
		Permissions: []string{
			CreateAction,
			ListAction,
			DeleteAction,
		},
	}

	PaymentFullAccessRuleGroup = ResourcePermission{
		Resource: PaymentResource,
		Permissions: []string{
			ListAction,
			DetailAction,
		},
	}

	OwnerRolePermissions = RolePermission{
		Role: OwnerRole,
		ResourcePermissions: []ResourcePermission{
			AccountFullAccessRuleGroup,
			ApiKeyFullAccessRuleGroup,
			BankAccountFullAccessRuleGroup,
			BalanceFullAccessRuleGroup,
			CashOutFullAccessRuleGroup,
			CheckoutSessionFullAccessRuleGroup,
			PaymentFullAccessRuleGroup,
		},
	}

	AdminRolePermissions = RolePermission{
		Role: AdminRole,
		ResourcePermissions: []ResourcePermission{
			AccountFullAccessRuleGroup,
			BankAccountFullAccessRuleGroup,
			BalanceFullAccessRuleGroup,
			CashOutFullAccessRuleGroup,
			CheckoutSessionFullAccessRuleGroup,
			PaymentFullAccessRuleGroup,
		},
	}
)
