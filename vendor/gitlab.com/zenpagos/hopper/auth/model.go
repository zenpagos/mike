package auth

type ResourcePermission struct {
	Resource    string
	Permissions []string
}

type RolePermission struct {
	Role                string
	ResourcePermissions []ResourcePermission
}
