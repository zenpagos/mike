package auth

import uuid "github.com/satori/go.uuid"

type AuthorizationManagerInterface interface {
	CreateRolesAndPermissions(accountID uuid.UUID) error
	AttachRole(actorID uuid.UUID, accountID uuid.UUID, role string) error
	Authorize(actorID uuid.UUID, accountID uuid.UUID, resource string, action string) (bool, error)
}
