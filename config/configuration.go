package config

import (
	"gitlab.com/zenpagos/mike/extsvc/dustinextsvc"
	"gitlab.com/zenpagos/mike/extsvc/elevenextsvc"
	"gitlab.com/zenpagos/mike/extsvc/hopperextsvc"
	"gitlab.com/zenpagos/mike/extsvc/lucasextsvc"
	"gitlab.com/zenpagos/mike/extsvc/maxineextsvc"
	"gitlab.com/zenpagos/mike/extsvc/willextsvc"
)

type Configuration struct {
	Service ServiceConfiguration
	Maxine  maxineextsvc.MaxineConfiguration
	Dustin  dustinextsvc.DustinConfiguration
	Lucas   lucasextsvc.LucasConfiguration
	Will    willextsvc.WillConfiguration
	Eleven  elevenextsvc.ElevenConfiguration
	Hopper  hopperextsvc.HopperConfiguration
}
