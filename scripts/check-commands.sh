#!/bin/bash

function error() {
  printf "\033[1;31m%s" "$1"
  printf "\033[0m\n"
  exit 1
}

if ! [ -x "$(command -v docker)" ]; then
  error "Error: docker is not installed"
fi

if ! [ -x "$(command -v helm)" ]; then
  error "Error: helm is not installed"
fi

if ! [ -x "$(command -v aws)" ]; then
  error "Error: aws is not installed"
fi
