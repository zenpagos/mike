#!/bin/bash

env=${ENV:-dev}
name=${NAME:-mike}
tag=${TAG:-latest}
registryURL=${REGISTRY_URL:-127.0.0.1:5000}

cat <<"EOF"

$$$$$$$$\
\____$$  |
    $$  / $$$$$$\  $$$$$$$\   $$$$$$\   $$$$$$\   $$$$$$\   $$$$$$\   $$$$$$$\
   $$  / $$  __$$\ $$  __$$\ $$  __$$\  \____$$\ $$  __$$\ $$  __$$\ $$  _____|
  $$  /  $$$$$$$$ |$$ |  $$ |$$ /  $$ | $$$$$$$ |$$ /  $$ |$$ /  $$ |\$$$$$$\
 $$  /   $$   ____|$$ |  $$ |$$ |  $$ |$$  __$$ |$$ |  $$ |$$ |  $$ | \____$$\
$$$$$$$$\\$$$$$$$\ $$ |  $$ |$$$$$$$  |\$$$$$$$ |\$$$$$$$ |\$$$$$$  |$$$$$$$  |
\________|\_______|\__|  \__|$$  ____/  \_______| \____$$ | \______/ \_______/
                             $$ |                $$\   $$ |
                             $$ |                \$$$$$$  |
                             \__|                 \______/

EOF

envDevKey=dev
envProdKey=prod

function info() {
  printf "\033[1;36m%s" "$1"
  printf "\033[0m\n"
}

function newLine() {
  printf "\n"
}

function error() {
  printf "\033[1;31m%s" "$1"
  printf "\033[0m\n"
  exit 1
}

# Check if is a valid environment
# Valid environments: dev and prod
if ! [[ "$env" =~ ^($envDevKey|$envProdKey)$ ]]; then
  error "$env is not a valid environment"
fi

# Print debug information
info "-----------------------------------------------------------------------------"
info "Service: $name"
info "Environment: $env"
info "Docker Image: $registryURL/$name:$tag"
info "-----------------------------------------------------------------------------"
newLine

if [ "$env" = "$envProdKey" ]; then
  info "--- Login in ECR ---"
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin "$registryURL" || {
    error "ECR login failed"
  }
fi

# Build docker image
info "--- Building Docker image ---"
docker build -t "$registryURL/$name:$tag" . || {
  error "Image build failed"
}

# Push image to registry
info "--- Pushing Docker image to registry ---"
docker push "$registryURL/$name:$tag" || {
  error "Push image to local registry failed"
}

info "--- Installing Helm chart ---"
if [ "$env" = "$envDevKey" ]; then
  helm upgrade --install \
    --set image.repository="$registryURL/$name" \
    --set image.tag="$tag" \
    -f scripts/mike-values.yaml \
    "$name" kubernetes/mike || {
    error "Install Helm chart failed"
  }
fi

if [ "$env" = "$envProdKey" ]; then
  helm upgrade --install \
    --set image.repository="$registryURL/$name" \
    --set image.tag="$tag" \
    "$name" kubernetes/mike || {
    error "Install Helm chart failed"
  }
fi
