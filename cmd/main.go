package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/mike/config"
	"gitlab.com/zenpagos/mike/container"
	"gitlab.com/zenpagos/mike/extsvc/dustinextsvc"
	"gitlab.com/zenpagos/mike/extsvc/elevenextsvc"
	"gitlab.com/zenpagos/mike/extsvc/hopperextsvc"
	"gitlab.com/zenpagos/mike/extsvc/lucasextsvc"
	"gitlab.com/zenpagos/mike/extsvc/maxineextsvc"
	"gitlab.com/zenpagos/mike/extsvc/willextsvc"
	"gitlab.com/zenpagos/mike/interface/rest"
)

func init() {
	log.SetReportCaller(true)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig()
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to external services
	mcl, mclConn, err := maxineextsvc.NewGRPCClient(conf.Maxine)
	if err != nil {
		log.Panicf("failed to connect to lucas service: %v", err)
	}
	defer mclConn.Close()

	dcl, dclConn, err := dustinextsvc.NewGRPCClient(conf.Dustin)
	if err != nil {
		log.Panicf("failed to connect to dustin service: %v", err)
	}
	defer dclConn.Close()

	lcl, lclConn, err := lucasextsvc.NewGRPCClient(conf.Lucas)
	if err != nil {
		log.Panicf("failed to connect to lucas service: %v", err)
	}
	defer lclConn.Close()

	wcl, wclConn, err := willextsvc.NewGRPCClient(conf.Will)
	if err != nil {
		log.Panicf("failed to connect to will service: %v", err)
	}
	defer wclConn.Close()

	ecl, eclConn, err := elevenextsvc.NewGRPCClient(conf.Eleven)
	if err != nil {
		log.Panicf("failed to connect to eleven service: %v", err)
	}
	defer eclConn.Close()

	hcl, hclConn, err := hopperextsvc.NewGRPCClient(conf.Hopper)
	if err != nil {
		log.Panicf("failed to connect to hopper service: %v", err)
	}
	defer hclConn.Close()

	c := container.InitializeContainer(mcl, dcl, lcl, wcl, ecl, hcl)
	r := rest.NewRestEngine(c)

	addr := fmt.Sprintf(":%d", conf.Service.Port)

	log.Tracef("init rest server in: %s", addr)

	if err := r.Run(addr); err != nil {
		log.Fatalf("failed to initialize the interface engine: %v", err)
	}
}
