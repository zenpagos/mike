//+build wireinject

package container

import (
	"github.com/google/wire"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	pbv1eleven "gitlab.com/zenpagos/eleven/proto/v1"
	pbv1hopper "gitlab.com/zenpagos/hopper/proto/v1"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

func InitializeContainer(
	mcl pbv1maxine.MaxineServiceClient,
	dcl pbv1dustin.DustinServiceClient,
	lcl pbv1lucas.LucasServiceClient,
	wcl pbv1will.WillServiceClient,
	ecl pbv1eleven.ElevenServiceClient,
	hcl pbv1hopper.HopperServiceClient,
) Container {
	wire.Build(NewContainer)

	return Container{}
}
