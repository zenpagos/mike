package container

import (
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	pbv1eleven "gitlab.com/zenpagos/eleven/proto/v1"
	pbv1hopper "gitlab.com/zenpagos/hopper/proto/v1"
	pbv1lucas "gitlab.com/zenpagos/lucas/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
)

type Container struct {
	MaxineClient pbv1maxine.MaxineServiceClient
	LucasClient  pbv1lucas.LucasServiceClient
	DustinClient pbv1dustin.DustinServiceClient
	WillClient   pbv1will.WillServiceClient
	ElevenClient pbv1eleven.ElevenServiceClient
	HopperClient pbv1hopper.HopperServiceClient
}

func NewContainer(
	mcl pbv1maxine.MaxineServiceClient,
	dcl pbv1dustin.DustinServiceClient,
	lcl pbv1lucas.LucasServiceClient,
	wcl pbv1will.WillServiceClient,
	ecl pbv1eleven.ElevenServiceClient,
	hcl pbv1hopper.HopperServiceClient,
) Container {
	return Container{
		MaxineClient: mcl,
		LucasClient:  lcl,
		DustinClient: dcl,
		WillClient:   wcl,
		ElevenClient: ecl,
		HopperClient: hcl,
	}
}
