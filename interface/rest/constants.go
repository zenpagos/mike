package rest

const (
	AccountIDHeaderKey     = "X-Account-ID"
	AuthorizationHeaderKey = "Authorization"
	ApiKeyHeaderKey        = "X-Api-Key"
	BearerAccessTokenKey   = "Bearer"
	CredentialTypeKey      = "credential_type"
	AccountIDKey           = "account_id"
	ApiKeyKey              = "api_key"
	AccessTokenKey         = "access_token"
)
