package rest

import "errors"

var (
	errUnauthorizedUser      = errors.New("unauthorized user")
	errInvalidCredentialType = errors.New("invalid credential type")
	errAccountIDRequired     = errors.New("account ID required")
	errCredentialRequired    = errors.New("credential required")
	errInvalidAccessToken    = errors.New("invalid access token")
	errTooManyCredentials    = errors.New("too many credentials, use: api-key or access token")
)
