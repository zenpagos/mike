package rest

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

func AccountIDExtractorMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		accountIDHeaderContent := c.Request.Header.Get(AccountIDHeaderKey)

		if accountIDHeaderContent == "" {
			c.AbortWithStatusJSON(
				http.StatusUnauthorized,
				gin.H{"error": errAccountIDRequired.Error()},
			)
			return
		}

		c.Set(AccountIDKey, accountIDHeaderContent)

		c.Next()
	}
}

func CredentialExtractorMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken := c.Request.Header.Get(AuthorizationHeaderKey)
		apiKey := c.Request.Header.Get(ApiKeyHeaderKey)

		// check for any credentials
		if accessToken == "" && apiKey == "" {
			c.AbortWithStatusJSON(
				http.StatusBadRequest,
				gin.H{"error": errCredentialRequired.Error()},
			)
			return
		}

		// check that just receive one type of credential
		if accessToken != "" && apiKey != "" {
			c.AbortWithStatusJSON(
				http.StatusBadRequest,
				gin.H{"error": errTooManyCredentials.Error()},
			)
			return
		}

		if accessToken != "" {
			fs := strings.Fields(accessToken)
			if fs[0] != BearerAccessTokenKey {
				c.AbortWithStatusJSON(
					http.StatusBadRequest,
					gin.H{"error": errInvalidAccessToken.Error()},
				)
				return
			}

			c.Set(CredentialTypeKey, AccessTokenKey)
			c.Set(AccessTokenKey, fs[1])
		}

		if apiKey != "" {
			c.Set(CredentialTypeKey, ApiKeyKey)
			c.Set(ApiKeyKey, apiKey)
		}

		c.Next()
	}
}
