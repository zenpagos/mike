package rest

import (
	"context"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	pbv1dustin "gitlab.com/zenpagos/dustin/proto/v1"
	pbv1eleven "gitlab.com/zenpagos/eleven/proto/v1"
	"gitlab.com/zenpagos/hopper/auth"
	pbv1hopper "gitlab.com/zenpagos/hopper/proto/v1"
	pbv1maxine "gitlab.com/zenpagos/maxine/proto/v1"
	"gitlab.com/zenpagos/mike/container"
	"gitlab.com/zenpagos/mike/model"
	"gitlab.com/zenpagos/tools"
	pbv1will "gitlab.com/zenpagos/will/proto/v1"
	"net/http"
	"time"
)

type rest struct {
	container container.Container
}

func NewRestEngine(c container.Container) *gin.Engine {
	r := gin.Default()
	s := &rest{container: c}

	// enable CORS
	r.Use(cors.New(cors.Config{
		AllowMethods: []string{"GET", "POST", "DELETE", "PUT"},
		AllowHeaders: []string{
			"Origin",
			"Content-Length",
			"Content-Type",
			"Authorization",
			"X-Account-ID",
			"X-Api-Key",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		MaxAge:           12 * time.Hour,
	}))

	v1 := r.Group("/v1")
	{
		v1.GET("/ping", s.pingHandler)
		v1.POST("/authenticate", s.requestAccessTokenHandler)
		v1.POST("/register", s.registerUserHandler)

		publicCheckout := v1.Group("/public/checkout")
		{
			publicCheckout.GET("/sessions/:id", s.retrievePublicCheckoutSessionHandler)
			publicCheckout.POST("/payment-methods", s.retrievePublicCheckoutPaymentMethodHandler)
		}

		userResources := v1.Use(CredentialExtractorMiddleware())
		{
			userResources.POST("/accounts", s.createAccountHandler)
		}

		accountResources := v1.Use(CredentialExtractorMiddleware(), AccountIDExtractorMiddleware())
		{
			accountResources.GET("/accounts", s.retrieveAccountHandler)
			accountResources.POST("/bank/accounts", s.createBankAccountHandler)
			accountResources.GET("/balances", s.listBalancesHandler)
			accountResources.POST("/cashouts", s.createCashOutHandler)
			accountResources.GET("/cashouts", s.listCashOutsHandler)
			accountResources.POST("/checkout/sessions", s.createCheckoutSessionHandler)
			accountResources.GET("/checkout/sessions", s.listCheckoutSessionsHandler)
			accountResources.GET("/payments", s.listPaymentsHandler)
		}
	}

	return r
}

func (r rest) isAuthorizedByAccessToken(accessToken string, accountID string, resource string, action string) error {
	req := &pbv1hopper.AuthorizeByAnAccessTokenRequest{
		AccessToken: accessToken,
		AccountID:   accountID,
		Resource:    resource,
		Action:      action,
	}

	res, err := r.container.HopperClient.AuthorizeByAnAccessToken(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		return errUnauthorizedUser
	}
	if !res.Result {
		log.WithError(err).Error()
		return errUnauthorizedUser
	}

	return nil
}

func (r rest) isAuthorizedByApiKey(apiKey string, accountID string, resource string, action string) error {
	req := &pbv1hopper.AuthorizeByAnApiKeyRequest{
		ApiKey:    apiKey,
		AccountID: accountID,
		Resource:  resource,
		Action:    action,
	}

	res, err := r.container.HopperClient.AuthorizeByAnApiKey(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		log.WithError(err).Error()
		return errUnauthorizedUser
	}
	if !res.Result {
		log.WithError(err).Error()
		return errUnauthorizedUser
	}

	return nil
}

func (r rest) checkCredentials(c *gin.Context, accountID string, resource string, action string) error {
	switch c.MustGet(CredentialTypeKey).(string) {
	case AccessTokenKey:
		accessToken := c.MustGet(AccessTokenKey).(string)
		return r.isAuthorizedByAccessToken(accessToken, accountID, resource, action)
	case ApiKeyKey:
		apiKey := c.MustGet(ApiKeyKey).(string)
		return r.isAuthorizedByApiKey(apiKey, accountID, resource, action)
	default:
		return errInvalidCredentialType
	}
}

func (r rest) pingHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "pong"})
}

func (r rest) registerUserHandler(c *gin.Context) {
	var u model.User

	if err := c.ShouldBindJSON(&u); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	req := &pbv1hopper.CreateUserRequest{
		FirstName:         u.FirstName,
		LastName:          u.LastName,
		Email:             u.Email,
		Password:          u.Password,
		MobilePhoneNumber: u.MobilePhoneNumber,
	}

	res, err := r.container.HopperClient.CreateUser(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	nu := model.User{
		ID:                res.ID,
		CreatedAt:         res.CreatedAt,
		FirstName:         res.FirstName,
		LastName:          res.LastName,
		Email:             res.Email,
		MobilePhoneNumber: res.MobilePhoneNumber,
	}

	c.JSON(http.StatusCreated, nu)
}

func (r rest) requestAccessTokenHandler(c *gin.Context) {
	var uc model.UserCredential

	if err := c.ShouldBindJSON(&uc); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	req := &pbv1hopper.AuthenticateRequest{
		Email:    uc.Email,
		Password: uc.Password,
	}

	res, err := r.container.HopperClient.Authenticate(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	nuc := model.AccessToken{
		AccessToken: res.AccessToken,
	}

	c.JSON(http.StatusOK, nuc)
}

func (r rest) createAccountHandler(c *gin.Context) {
	var a model.Account

	// TODO: validate the access token with hopper service, only a valid user could create accounts.

	if err := c.ShouldBindJSON(&a); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	createAccountReq := &pbv1will.CreateAccountRequest{
		Name:                a.Name,
		TIN:                 a.TIN,
		SupportEmail:        a.SupportEmail,
		SupportPhoneNumber:  a.SupportPhoneNumber,
		StatementDescriptor: a.StatementDescriptor,
		UserID:              "939db672-c022-466a-b542-c2213404c1bb", // TODO: hardcoded!! get from hopper
	}

	createAccountRes, err := r.container.WillClient.CreateAccount(context.Background(), createAccountReq)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	accessToken := c.MustGet(AccessTokenKey).(string)
	createAccountOwnerPoliciesReq := &pbv1hopper.AttachFullAccessPoliciesRequest{
		AccessToken: accessToken,
		AccountID:   createAccountRes.Account.ID,
	}

	createAccountOwnerPoliciesRes, err := r.container.HopperClient.AttachFullAccessPolicies(
		context.Background(),
		createAccountOwnerPoliciesReq,
	)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// TODO: check the result, otherwise rollback the operation.
	_ = createAccountOwnerPoliciesRes

	na := model.Account{
		ID:                  createAccountRes.Account.ID,
		CreatedAt:           createAccountRes.Account.CreatedAt,
		Name:                createAccountRes.Account.Name,
		TIN:                 createAccountRes.Account.TIN,
		SupportEmail:        createAccountRes.Account.SupportEmail,
		SupportPhoneNumber:  createAccountRes.Account.SupportPhoneNumber,
		StatementDescriptor: createAccountRes.Account.StatementDescriptor,
	}

	c.JSON(http.StatusCreated, na)
}

func (r rest) retrieveAccountHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.AccountResource, auth.DetailAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	req := &pbv1will.GetAccountDetailRequest{
		ID: accountID,
	}

	res, err := r.container.WillClient.GetAccountDetail(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	na := model.Account{
		ID:                  res.Account.ID,
		CreatedAt:           res.Account.CreatedAt,
		Name:                res.Account.Name,
		TIN:                 res.Account.TIN,
		SupportEmail:        res.Account.SupportEmail,
		SupportPhoneNumber:  res.Account.SupportPhoneNumber,
		StatementDescriptor: res.Account.StatementDescriptor,
	}

	c.JSON(http.StatusOK, na)
}

func (r rest) createBankAccountHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.BankAccountResource, auth.CreateAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	var ba model.BankAccount

	if err := c.ShouldBindJSON(&ba); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	req := &pbv1will.CreateBankAccountRequest{
		CBU:         ba.CBU,
		Description: ba.Description,
		AccountID:   accountID,
	}

	res, err := r.container.WillClient.CreateBankAccount(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	nba := model.BankAccount{
		ID:          res.ID,
		CreatedAt:   res.CreatedAt,
		CBU:         res.CBU,
		Description: res.Description,
		AccountID:   res.AccountID,
	}

	c.JSON(http.StatusCreated, nba)
}

func (r rest) listBalancesHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.BalanceResource, auth.ListAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	req := &pbv1maxine.ListBalancesRequest{
		AccountID: accountID,
	}

	res, err := r.container.MaxineClient.ListBalances(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	bls := make([]model.Balance, 0)
	for _, b := range res.Balances {
		bls = append(bls, model.Balance{
			Type:   b.Type,
			Amount: b.Amount,
		})
	}

	c.JSON(http.StatusOK, bls)
}

func (r rest) createCashOutHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.CashOutResource, auth.CreateAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	var co model.CashOut
	if err := c.ShouldBindJSON(&co); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	req := &pbv1eleven.CreateCashOutRequest{
		AccountID:     accountID,
		BankAccountID: co.BankAccountID,
		Amount:        co.Amount,
	}

	res, err := r.container.ElevenClient.CreateCashOut(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	nco := model.CashOut{
		ID:            res.ID,
		CreatedAt:     res.CreatedAt,
		Status:        res.Status,
		AccountID:     res.AccountID,
		BankAccountID: res.BankAccountID,
		Amount:        res.Amount,
	}

	c.JSON(http.StatusCreated, nco)
}

func (r rest) listCashOutsHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.CashOutResource, auth.ListAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	req := &pbv1eleven.ListCashOutsRequest{
		AccountID: accountID,
	}

	res, err := r.container.ElevenClient.ListCashOuts(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cos := make([]model.CashOut, 0)
	for _, co := range res.CashOuts {
		cos = append(cos, model.CashOut{
			ID:            co.ID,
			CreatedAt:     co.CreatedAt,
			UpdatedAt:     co.UpdatedAt,
			Status:        co.Status,
			AccountID:     co.AccountID,
			BankAccountID: co.BankAccountID,
			Amount:        co.Amount,
		})
	}

	c.JSON(http.StatusOK, cos)
}

func (r rest) createCheckoutSessionHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.CheckoutSessionResource, auth.CreateAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	var cs model.CheckoutSession
	if err := c.ShouldBindJSON(&cs); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	csi := make([]*pbv1dustin.CheckoutSessionItem, 0)
	for _, i := range cs.Items {
		csi = append(csi, &pbv1dustin.CheckoutSessionItem{
			Code:      tools.StringValueOrBlank(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringValueOrBlank(i.ItemURL),
			ImageURL:  tools.StringValueOrBlank(i.ImageURL),
		})
	}

	req := &pbv1dustin.CreateCheckoutSessionRequest{
		AccountID:   accountID,
		Currency:    cs.Currency,
		Reference:   cs.Reference,
		Description: cs.Description,
		Items:       csi,
	}

	res, err := r.container.DustinClient.CreateCheckoutSession(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	rcs := res.CheckoutSession

	ncsi := make([]model.CheckoutSessionItem, 0)
	for _, i := range rcs.Items {
		ncsi = append(ncsi, model.CheckoutSessionItem{
			Code:      tools.StringPointerOrNil(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringPointerOrNil(i.ItemURL),
			ImageURL:  tools.StringPointerOrNil(i.ImageURL),
		})
	}

	ncs := model.CheckoutSession{
		ID:          rcs.ID,
		CreatedAt:   rcs.CreatedAt,
		UpdatedAt:   tools.Int64PointerOrNil(rcs.UpdatedAt),
		ExpiresAt:   tools.Int64PointerOrNil(rcs.ExpiresAt),
		Status:      rcs.Status,
		Total:       rcs.Total,
		AccountID:   rcs.AccountID,
		Currency:    rcs.Currency,
		Reference:   rcs.Reference,
		Description: rcs.Description,
		Items:       ncsi,
	}

	c.JSON(http.StatusCreated, ncs)
}

func (r rest) listCheckoutSessionsHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.CheckoutSessionResource, auth.ListAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	req := &pbv1dustin.ListCheckoutSessionsRequest{
		AccountID: accountID,
	}

	res, err := r.container.DustinClient.ListCheckoutSessions(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	css := make([]model.CheckoutSession, 0)
	for _, cs := range res.CheckoutSessions {

		csi := make([]model.CheckoutSessionItem, 0)
		for _, i := range cs.Items {
			csi = append(csi, model.CheckoutSessionItem{
				Code:      tools.StringPointerOrNil(i.Code),
				Name:      i.Name,
				Quantity:  i.Quantity,
				UnitPrice: i.UnitPrice,
				ItemURL:   tools.StringPointerOrNil(i.ItemURL),
				ImageURL:  tools.StringPointerOrNil(i.ImageURL),
			})
		}

		css = append(css, model.CheckoutSession{
			ID:          cs.ID,
			CreatedAt:   cs.CreatedAt,
			UpdatedAt:   tools.Int64PointerOrNil(cs.UpdatedAt),
			ExpiresAt:   tools.Int64PointerOrNil(cs.ExpiresAt),
			Status:      cs.Status,
			Total:       cs.Total,
			AccountID:   cs.AccountID,
			Currency:    cs.Currency,
			Reference:   cs.Reference,
			Description: cs.Description,
			Items:       csi,
		})
	}

	c.JSON(http.StatusOK, css)
}

func (r rest) listPaymentsHandler(c *gin.Context) {
	accountID := c.MustGet(AccountIDKey).(string)
	if err := r.checkCredentials(c, accountID, auth.PaymentResource, auth.ListAction); err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errUnauthorizedUser.Error()})
		return
	}

	req := &pbv1eleven.ListPaymentsRequest{
		AccountID: accountID,
	}

	res, err := r.container.ElevenClient.ListPayments(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ps := make([]model.Payment, 0)
	for _, p := range res.Payments {
		ps = append(ps, model.Payment{
			ID:                  p.ID,
			CreatedAt:           p.CreatedAt,
			Status:              p.Status,
			CheckoutSessionID:   p.CheckoutSessionID,
			PaymentMethodType:   p.PaymentMethodType,
			Installments:        p.Installments,
			PayerTIN:            p.PayerTIN,
			PayerEmail:          p.PayerEmail,
			CardExpirationYear:  p.CardExpirationYear,
			CardExpirationMonth: p.CardExpirationMonth,
			CardHolderName:      p.CardHolderName,
		})
	}

	c.JSON(http.StatusOK, ps)
}

func (r rest) retrievePublicCheckoutSessionHandler(c *gin.Context) {
	checkoutSessionID := c.Param("id")

	req := &pbv1dustin.GetPendingCheckoutSessionRequest{
		ID: checkoutSessionID,
	}

	res, err := r.container.DustinClient.GetPendingCheckoutSession(context.Background(), req)
	if err != nil {
		log.WithError(err).Error()
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	csi := make([]model.CheckoutSessionItem, 0)
	for _, i := range res.CheckoutSession.Items {
		csi = append(csi, model.CheckoutSessionItem{
			Code:      tools.StringPointerOrNil(i.Code),
			Name:      i.Name,
			Quantity:  i.Quantity,
			UnitPrice: i.UnitPrice,
			ItemURL:   tools.StringPointerOrNil(i.ItemURL),
			ImageURL:  tools.StringPointerOrNil(i.ImageURL),
		})
	}

	cs := model.CheckoutSession{
		ID:          res.CheckoutSession.ID,
		Total:       res.CheckoutSession.Total,
		Reference:   res.CheckoutSession.Reference,
		Description: res.CheckoutSession.Description,
		Items:       csi,
	}

	c.JSON(http.StatusOK, cs)
}

func (r rest) retrievePublicCheckoutPaymentMethodHandler(c *gin.Context) {
	type request struct {
		CheckoutSessionID string `json:"checkout_session_id"`
		PaymentMethod     string `json:"payment_method"`
		PaymentMethodType string `json:"payment_method_type"`
	}

	req := new(request)
	if err := c.ShouldBindJSON(&req); err != nil {
		log.WithError(err).Error()
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	getSessionReq := &pbv1dustin.GetPendingCheckoutSessionRequest{
		ID: req.CheckoutSessionID,
	}
	cs, err := r.container.DustinClient.GetPendingCheckoutSession(context.Background(), getSessionReq)
	if err != nil {
		log.WithError(err).Error()
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	getCheckoutPaymentMethodReq := &pbv1will.GetCheckoutPaymentMethodRequest{
		AccountID:         cs.CheckoutSession.AccountID,
		Amount:            cs.CheckoutSession.Total,
		PaymentMethod:     req.PaymentMethod,
		PaymentMethodType: req.PaymentMethodType,
	}
	cpm, err := r.container.WillClient.GetCheckoutPaymentMethod(context.Background(), getCheckoutPaymentMethodReq)
	if err != nil {
		log.WithError(err).Error()
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cir := make([]*model.CheckoutInstallment, 0)
	for _, ci := range cpm.CheckoutInstallment {
		cir = append(cir, &model.CheckoutInstallment{
			Installments:      ci.Installments,
			InstallmentAmount: ci.InstallmentAmount,
			AmountToPay:       ci.AmountToPay,
		})
	}

	cpmr := &model.CheckoutPaymentMethod{
		Name:         cpm.Name,
		Type:         cpm.Type,
		LogoURL:      cpm.LogoURL,
		Installments: cir,
	}

	c.JSON(http.StatusOK, cpmr)
}
