package model

type BankAccount struct {
	ID          string `json:"id"`
	CreatedAt   int64  `json:"created_at"`
	CBU         string `json:"cbu" binding:"required"`
	Description string `json:"description,omitempty"`
	AccountID   string `json:"account_id"`
}
