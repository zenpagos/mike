package model

type CheckoutPaymentMethod struct {
	Name         string                 `json:"name"`
	Type         string                 `json:"type"`
	LogoURL      string                 `json:"logo_url"`
	Installments []*CheckoutInstallment `json:"installments"`
}
