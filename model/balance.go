package model

type Balance struct {
	Type   string `json:"type"`
	Amount int64  `json:"amount"`
}
