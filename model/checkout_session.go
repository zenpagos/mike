package model

type CheckoutSession struct {
	ID          string                `json:"id,omitempty"`
	CreatedAt   int64                 `json:"created_at,omitempty"`
	UpdatedAt   *int64                `json:"updated_at,omitempty"`
	ExpiresAt   *int64                `json:"expires_at,omitempty"`
	Status      string                `json:"status,omitempty"`
	Total       int64                 `json:"total,omitempty"`
	AccountID   string                `json:"account_id,omitempty"`
	Currency    string                `json:"currency,omitempty" binding:"required"`
	Reference   string                `json:"reference,omitempty" binding:"required"`
	Description string                `json:"description,omitempty" binding:"required"`
	Items       []CheckoutSessionItem `json:"items,omitempty" binding:"required"`
}
