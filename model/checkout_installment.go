package model

type CheckoutInstallment struct {
	Installments      int64 `json:"installments"`
	InstallmentAmount int64 `json:"installment_amount"`
	AmountToPay       int64 `json:"amount_to_pay"`
}
