package model

type CashOut struct {
	ID            string `json:"id"`
	CreatedAt     int64  `json:"created_at"`
	UpdatedAt     int64  `json:"updated_at,omitempty"`
	Status        string `json:"status"`
	AccountID     string `json:"account_id"`
	BankAccountID string `json:"bank_account_id" binding:"required"`
	Amount        int64  `json:"amount" binding:"required"`
}
