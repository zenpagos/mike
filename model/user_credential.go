package model

type UserCredential struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
