package model

type User struct {
	ID                string `json:"id"`
	CreatedAt         int64  `json:"created_at"`
	FirstName         string `json:"first_name" binding:"required"`
	LastName          string `json:"last_name" binding:"required"`
	Email             string `json:"email" binding:"required,email"`
	Password          string `json:"password,omitempty" binding:"required"`
	MobilePhoneNumber string `json:"mobile_phone_number,omitempty"`
}
