package model

type Account struct {
	ID                  string `json:"id"`
	CreatedAt           int64  `json:"created_at"`
	UpdatedAt           int64  `json:"updated_at,omitempty"`
	Name                string `json:"name" binding:"required"`
	TIN                 string `json:"tin" binding:"required"`
	SupportEmail        string `json:"support_email,omitempty"`
	SupportPhoneNumber  string `json:"support_phone_number,omitempty"`
	StatementDescriptor string `json:"statement_descriptor,omitempty"`
}
