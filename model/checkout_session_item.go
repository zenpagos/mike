package model

type CheckoutSessionItem struct {
	Code      *string `json:"code,omitempty"`
	Name      string  `json:"name"`
	Quantity  int64   `json:"quantity"`
	UnitPrice int64   `json:"unit_price"`
	ItemURL   *string `json:"item_url,omitempty"`
	ImageURL  *string `json:"image_url,omitempty"`
}
