package model

type Payment struct {
	ID                  string `json:"id"`
	CreatedAt           int64  `json:"created_at"`
	Status              string `json:"status"`
	CheckoutSessionID   string `json:"checkout_session_id"`
	PaymentMethodType   string `json:"payment_method_type"`
	Installments        int64  `json:"installments"`
	PayerTIN            string `json:"payer_tin"`
	PayerEmail          string `json:"payer_email"`
	CardExpirationYear  int64  `json:"card_expiration_year"`
	CardExpirationMonth int64  `json:"card_expiration_month"`
	CardHolderName      string `json:"card_holder_name"`
}
