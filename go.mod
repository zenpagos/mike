module gitlab.com/zenpagos/mike

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/google/wire v0.4.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	gitlab.com/zenpagos/dustin v0.0.0-20200616235945-be81061286dd
	gitlab.com/zenpagos/eleven v0.0.0-20200617001056-fa7e91556fe3
	gitlab.com/zenpagos/hopper v0.0.0-20200616235240-9b874f793dc2
	gitlab.com/zenpagos/lucas v0.0.0-20200616233439-4e7d2c50f96c
	gitlab.com/zenpagos/maxine v0.0.0-20200616231236-18596cd11ccb
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	gitlab.com/zenpagos/will v0.0.0-20200617000436-69386059708f
	golang.org/x/net v0.0.0-20200528225125-3c3fba18258b // indirect
	google.golang.org/genproto v0.0.0-20200528191852-705c0b31589b // indirect
	google.golang.org/grpc v1.29.1
)
