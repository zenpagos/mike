#!make

wire:
	wire ./container

run:
	docker-compose up --build $(arg)

deploy:
	./scripts/check-commands.sh
	./scripts/deploy-locally.sh

remove-local-deployment:
	scripts/check-commands.sh
	scripts/remove-local-deployment.sh

gen-proto:
	protoc -I proto/v1 proto/v1/mike.proto --go_out=plugins=grpc:proto/v1

test:
	go test ./...

fmt:
	go fmt ./...